Template.home.helpers({
    friends: function () {
        if(Meteor.user().profile){
            var friends = Meteor.user().profile.friends;
            var friendsList = [];
            
            for (varr i = 0; i <friends.length; i++){
                friendList.push(Meteor.users.findOne(friends[i]));
            }
            
            return friendsList;
        }
    },
    yourGames: functions () {
        var games = Games.find({
            players: {
                $elemMatch: {
                    player: Meteor.userId()
                }

            },
            turn: Meteor.userId(),
            gameStatus: "inProgress"
        });
        
        return games;
    },
        opponentData: function () {
        var playerIndex;
            
            if(this.player[0].player === Meteor.userId()){
                playerIndex = 1;
            } else {
                playerIndex = 0;
            }
            
            return Meteor.users.findOne(this.players[playerIndex].player);
        },
        opponentScore: function () {
            var playerIndex;
            
            if(this.players[0].player === Meteor.userId()){
                playerIndex = 1;
            } else {
                playerIndex = 0;
            }
            
            return this.players[playerIndex].score;
        }
});

Template.home.events({
    "click .rand-user": function () {
        var randomUser, canidatePool;

        canidatePool = Meteor.users.find().fetch();

        for (var i = 0; i < canidatePool.length; i++) {
            if (canidatePool[i]._id === Meteor.userId()) {
                canidatePool.splice(i, 1);
                break;
            }
        }

        randomUser = _.sample(canidatePool);

        var game = Game.insert({
            players: [
                {
                    player: Meteor.userId(),
                    score: 0
            },
                {
                    player: randomUser._id,
                    score: 0
            }
            ],
            turn: Meteor.userId(),
            round: 0,
            gameStatus: "inProgress"
        });

        Router.go('game', {
            _id: game
        })
    },
    "click .challenge-friend": function () {
        var game = Games.insert({
            players: [
                {
                    player: Meteor.userId(),
                    score: 0
                },
                {
                    player: this._id,
                    score: 0
                }
            ],
            turn: Meteor.userId(),
            round: 0,
            gameStatus: "inProgress"
        });
        Router.go('game', {
            _id: game
        });
    }
    
});